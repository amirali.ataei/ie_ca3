<%@ page import="com.example.CA3.java.Course" %>
<%@ page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Plan</title>
    <style>
        table{
            width: 100%;
            text-align: center;

        }
        table, th, td{
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<%
    String studentId = (String) request.getAttribute("studentId");
    String[] times = (String[]) request.getAttribute("times");
    String[] sat = (String[]) request.getAttribute("saturday");
    String[] sun = (String[]) request.getAttribute("sunday");
    String[] mon = (String[]) request.getAttribute("monday");
    String[] tue = (String[]) request.getAttribute("tuesday");
    String[] wed = (String[]) request.getAttribute("wednesday");
%>

<body>
    <a href="/">Home</a>
    <li id="code">Student Id: <%=studentId%></li>
    <br>
    <table>
        <tr>
            <th></th>
            <% for(int i = 0; i < 5; i++) { %>
                <th><%=times[i]%></th>
            <%}%>
        </tr>
        <tr>
            <td>Saturday</td>
            <% for(int i = 0; i < 5; i++) { %>
                <td><%=sat[i]%></td>
            <%}%>
        </tr>
        <tr>
            <td>Sunday</td>
            <% for(int i = 0; i < 5; i++) { %>
                <td><%=sun[i]%></td>
            <%}%>
        </tr>
        <tr>
            <td>Monday</td>
            <% for(int i = 0; i < 5; i++) { %>
                <td><%=mon[i]%></td>
            <%}%>
        </tr>
        <tr>
            <td>Tuesday</td>
            <% for(int i = 0; i < 5; i++) { %>
                <td><%=tue[i]%></td>
            <%}%>
        </tr>
        <tr>
            <td>Wednesday</td>
            <% for(int i = 0; i < 5; i++) { %>
                <td><%=wed[i]%></td>
            <%}%>
        </tr>
    </table>
</body>
</html>