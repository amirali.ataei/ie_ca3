package com.example.CA3.java;

import static com.example.CA3.java.Tools.getCourse;

public class Grade {
    private String code;
    private double grade;

    public Grade(String _code, double _grade) {
        code = _code;
        grade = _grade;
    }

    public String getCode() {
        return code;
    }

    public double getGrade() {
        return grade;
    }

    public int getUnits() {
        int units = 0;
        try {
            units = getCourse(code, "01").getUnits();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return units;
    }
}
