package com.example.CA3.java;

import org.json.simple.JSONObject;

import java.util.ArrayList;

public class Student {
    private String id;
    private String name;
    private String secondName;
    private String birthDate;
//    private String enteredAt;

    private ArrayList<Grade> courses = new ArrayList<>();

    private WeeklySchedule weeklySchedule = new WeeklySchedule(this);

    public Student(JSONObject jsonObject) {
        id = (String) jsonObject.get("id");
        name = (String) jsonObject.get("name");
        secondName = (String) jsonObject.get("secondName");
        birthDate = (String) jsonObject.get("birthDate");
//        enteredAt = (String) jsonObject.get("enteredAt");

        weeklySchedule = new WeeklySchedule(this);
    }

    public Student() {

    }

    public boolean hadCourse(String code) {
        for(Grade course : courses) {
            if(code.equals(course.getCode()))
                return true;
        }
        return false;
    }

    public double getGrade(String code) {
        for(Grade course : courses) {
            if(course.getCode().equals(code))
                return course.getGrade();
        }
        return 0;
    }

    public WeeklySchedule getWeeklySchedule() {
        return weeklySchedule;
    }

    public String getStudentId() {
        return id;
    }
    public String getName() { return name; }
    public String getSecondName() { return secondName; }
    public String getBirthDate() { return birthDate; }

    public double getGPA() {
        double GPA = 0.0;
        for(Grade course: courses) {
            double grade = course.getGrade();
            GPA += grade;
        }

        GPA = GPA / courses.size();

        return GPA;

    }

    public int getNumOfPassedUnits() {
        int passedUnits = 0;
        for(Grade course : courses) {
            double grade = course.getGrade();
            int units = course.getUnits();
            if(grade >= 10)
                passedUnits += units;
        }
        return passedUnits;
    }

    public ArrayList<Grade> getPassedCourses() {
        ArrayList<Grade> passedCourses = new ArrayList<>();
        for(Grade course : courses) {
            double grade = course.getGrade();
            if(grade >= 10)
                passedCourses.add(course);
        }
        return passedCourses;
    }

    public void addCourse(Grade course) {
        courses.add(course);
    }

    public void addToWeeklySchedule(Course course) throws Exception{
        weeklySchedule.aTWS(course);
    }

    public void removeFromWeeklySchedule(Course course) {
        this.weeklySchedule.rFWS(course);
    }

    public ArrayList<Course> getWeeklyScheduleCourses(){
        return weeklySchedule.gWS();
    }

    public void finalizeWeeklySchedule() throws Exception{
        this.weeklySchedule.fWS();
    }

    public void setWeeklySchedule(ArrayList<Course> courses) {
        weeklySchedule.setWS(courses);
    }

}
