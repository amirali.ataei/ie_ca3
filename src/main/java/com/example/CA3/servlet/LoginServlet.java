package com.example.CA3.servlet;

import com.example.CA3.java.Grade;
import com.example.CA3.java.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

import static com.example.CA3.java.Tools.*;

@WebServlet(name = "LoginServlet", value = "/login")
public class LoginServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            courses = getCourses();
            students = getStudents();
            for(Student student:students) {
                ArrayList<Grade> grades = getGrades(student.getStudentId());
                for (Grade course : grades) {
                    student.addCourse(course);
                }
            }
        }
        catch (Exception e) {}

        request.getRequestDispatcher("/Login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
