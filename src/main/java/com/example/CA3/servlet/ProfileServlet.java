package com.example.CA3.servlet;

import com.example.CA3.java.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

import static com.example.CA3.java.Tools.*;

@WebServlet(name = "ProfileServlet", value = "/profile")
public class ProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(studentId.equals(""))
            request.getRequestDispatcher("/login").forward(request, response);

        Student student = null;
        try {
            student = getStudent(studentId);
        } catch (Exception e) {
            e.printStackTrace();
        }


        request.setAttribute("student", student);
        assert student != null;
        request.setAttribute("passedCourses", student.getPassedCourses());
        request.getRequestDispatcher("/Profile.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
