package com.example.CA3.servlet;

import com.example.CA3.java.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

import static com.example.CA3.java.Tools.*;

@WebServlet(name = "MainServlet", value = "")
public class MainServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        selectedCourses = new ArrayList<>(lastSubmit);

        if(request.getParameter("std_id") != null)
            studentId = request.getParameter("std_id");

        if(studentId.equals(""))
            request.getRequestDispatcher("/login").forward(request, response);

        Student student = null;

        try {
            student = getStudent(studentId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        student.setWeeklySchedule(selectedCourses);

        request.setAttribute("studentId", studentId);
        request.getRequestDispatcher("/Home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
