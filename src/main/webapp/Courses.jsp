<%@ page import="com.example.CA3.java.Tools" %>
<%@ page import="com.example.CA3.java.Student" %>
<%@ page import="com.example.CA3.java.WeeklySchedule" %>
<%@ page import="com.example.CA3.java.Course" %>
<%@ page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Courses</title>
    <style>
        .course_table {
            width: 100%;
            text-align: center;
        }
        .search_form {
            text-align: center;
        }
    </style>
</head>
<%

    String studentId = (String) request.getAttribute("studentId");
    String searchFilter = (String) request.getAttribute("searchFilter");

    int units = (Integer) request.getAttribute("totalUnits");
    ArrayList<Course> selectedCourses = (ArrayList<Course>) request.getAttribute("selectedCourses");
    ArrayList<Course> filterCourses = (ArrayList<Course>) request.getAttribute("filterCourses");
%>
<body>
    <a href="/">Home</a>
    <li id="code">Student Id: <%=studentId%></li>
    <li id="units">Total Selected Units: <%=units%></li>

    <br>

    <table>
        <tr>
            <th>Code</th>
            <th>Class Code</th>
            <th>Name</th>
            <th>Units</th>
            <th></th>
        </tr>
        <%for(Course course : selectedCourses){%>
        <tr>
            <td><%=course.getCode()%></td>
            <td><%=course.getClassCode()%></td>
            <td><%=course.getName()%></td>
            <td><%=course.getUnits()%></td>
            <td>
                <form action="" method="POST" >
                    <input id="form_action" type="hidden" name="action" value="remove">
                    <input id="form_course_code" type="hidden" name="course_code" value=<%=course.getCode()%>>
                    <input id="form_class_code" type="hidden" name="class_code" value=<%=course.getClassCode()%>>
                    <button type="submit">Remove</button>
                </form>
            </td>
        </tr>
        <%}%>
    </table>

    <br>

    <form action="" method="POST">
        <button type="submit" name="action" value="submit">Submit Plan</button>
        <button type="submit" name="action" value="reset">Reset</button>
    </form>

    <br>

    <form class="search_form" action="" method="POST">
        <label>Search:</label>
        <input type="text" name="search" value=<%=searchFilter%>>
        <button type="submit" name="action" value="search">Search</button>
        <button type="submit" name="action" value="clear">Clear Search</button>
    </form>

    <br>

    <table class="course_table">
        <tr>
            <th>Code</th>
            <th>Class Code</th>
            <th>Name</th>
            <th>Units</th>
            <th>Signed Up</th>
            <th>Capacity</th>
            <th>Type</th>
            <th>Days</th>
            <th>Time</th>
            <th>Exam Start</th>
            <th>Exam End</th>
            <th>Prerequisites</th>
            <th></th>
        </tr>
        <%for(Course course : filterCourses){%>
        <tr>
            <td><%=course.getCode()%></td>
            <td><%=course.getClassCode()%></td>
            <td><%=course.getName()%></td>
            <td><%=course.getUnits()%></td>
            <td><%=course.getSignedUp()%></td>
            <td><%=course.getCapacity()%></td>
            <td><%=course.getType()%></td>
            <td><%=course.getClassTime().get("days")%></td>
            <td><%=course.getClassTime().get("time")%></td>
            <td><%=course.getExamTime().get("start")%></td>
            <td><%=course.getExamTime().get("end")%></td>
            <td><%=course.getPrerequisites()%></td>
            <td>
                <form action="" method="POST" >
                    <input id="form_action1" type="hidden" name="action" value="add">
                    <input id="form_course_code1" type="hidden" name="course_code" value=<%=course.getCode()%>>
                    <input id="form_class_code1" type="hidden" name="class_code" value=<%=course.getClassCode()%>>
                    <button type="submit">Add</button>
                </form>
            </td>
        </tr>
        <%}%>
    </table>

</body>
</html>